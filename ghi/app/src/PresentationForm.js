import React from 'react';

class PresentationForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            presenterName: "",
            presenterEmail: "",
            companyName: "",
            title: "",
            synopsis: "",
            conferences: [],
        };
    };

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        data.presenter_name = data.presenterName;
        data.presenter_email = data.presenterEmail;
        data.company_name = data.companyName;
        delete data.presenterName;
        delete data.presenterEmail;
        delete data.companyName;
        delete data.conferences;

        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch (presentationUrl, fetchConfig);
        if(response.ok){
            const newPresentation = await response.json()

            const cleared = {
                presenterName: "",
                presenterEmail: "",
                companyName: "",
                title: "",
                synopsis: "",
                conference: "",
            }
            this.setState(cleared)
        }
    }

    handlePresenterNameChange = (event) => {
        const value = event.target.value;
        this.setState({presenterName: value})

    }

    handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        this.setState({presenterEmail: value})

    }

    handleCompanyNameChange = (event) => {
        const value = event.target.value;
        this.setState({companyName: value})

    }

    handleTitleChange = (event) => {
        const value = event.target.value;
        this.setState({title: value})

    }

    handleSynopsisChange = (event) => {
        const value = event.target.value;
        this.setState({synopsis: value})

    }

    handleConferenceChange = (event) => {
        const value = event.target.value;
        this.setState({conference: value})

    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        console.log(response)
        if (response.ok){
            const data = await response.json();
            this.setState({conferences:data.conferences});
        }
    }
    render(){
    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterNameChange} placeholder="Presenter name" type="text" name="presenter_name" id="presenter_name" value={this.state.presenterName} className="form-control"/>
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterEmailChange} placeholder="Presenter email" type="email" name="presenter_email" id="presenter_email" value={this.state.presenterEmail} className="form-control"/>
                <label htmlFor="name">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" value={this.state.companyName} className="form-control"/>
                <label htmlFor="name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTitleChange} placeholder="Title" type="text" name="title" id="title" value={this.state.title} className="form-control"/>
                <label htmlFor="name">Title</label>
              </div>
              <div className="mb-3">
                <textarea onChange={this.handleSynopsisChange} placeholder="Synopsis" name="synopsis" id="synopsis" rows ="5" value={this.state.synopsis} className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={this.handleConferenceChange} name="conferences" id="conferences" value={this.state.conference} className="form-select">
                  <option  value="">Choose a conference</option>
                  {this.state.conferences.map(conference => {
                        return(
                            <option key={conference.href} value={conference.id}>
                                {conference.name}
                            </option>
                        )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
          </div>
          </div>
          )
        }
}

export default PresentationForm
