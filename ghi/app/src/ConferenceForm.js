import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            name: "",
            startDate: "",
            endDate: "",
            description: "",
            maxPresentations: "",
            maxAttendees: "",
            locations: [],
            location: ""
        };

        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleStartDateChange = this.handleStartDateChange.bind(this)
        this.handleEndDateChange = this.handleEndDateChange.bind(this)
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this)
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }



    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.starts = data.startDate;
        data.ends = data.endDate;
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.startDate;
        delete data.endDate;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch (conferenceUrl, fetchConfig);
        if (response.ok){
            const newConference = await response.json()
            console.log(newConference)

            const cleared = {
                name: "",
                startDate: "",
                endDate: "",
                description: "",
                maxPresentations: "",
                maxAttendees: "",
                location: "",
            }
            this.setState(cleared)
        }
    }

    handleNameChange(event){
        const value = event.target.value;
        this.setState({name: value})
    }

    handleStartDateChange(event){
        const value = event.target.value;
        this.setState({startDate: value})
    }

    handleEndDateChange(event){
        const value = event.target.value;
        this.setState({endDate: value})
    }

    handleDescriptionChange(event){
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentationsChange(event){
        const value = event.target.value;
        this.setState({maxPresentations: value})
    }

    handleMaxAttendeesChange(event){
        const value = event.target.value;
        this.setState({maxAttendees: value})
    }

    handleLocationChange(event){
        const value = event.target.value;
        this.setState({location: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({locations:data.locations});
        }
    }

    render(){
        return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" value={this.state.name} className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleStartDateChange} placeholder="Start Date" required type="date" name="starts" id="starts" value={this.state.startDate} className="form-control"/>
                <label htmlFor="start_date">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleEndDateChange} placeholder="End Date"  required type="date" name="ends" id="ends" value={this.state.endDate} className="form-control"/>
                <label htmlFor="end_date">End date</label>
              </div>
              <div className="mb-3">
                <textarea onChange = {this.handleDescriptionChange} placeholder="Description" required name="description" id="description" value={this.state.description} rows ="5" className="form-control"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleMaxPresentationsChange}placeholder="Max presentations"  required type="number" name="max_presentations" id="max_presentations" value={this.state.maxPresentations} className="form-control"/>
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleMaxAttendeesChange} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" value={this.state.maxAttendees} className="form-control"/>
                <label htmlFor="max_attendees">Max attendees</label>
              </div>
              <div className="mb-3">
                <select  onChange={this.handleLocationChange} required name="location" id="location" value={this.state.location} className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return(
                        <option key={location.id} value={location.id}>
                                {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        )
    }

}
export default ConferenceForm
