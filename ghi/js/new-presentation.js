window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok){
        const data = await response.json();
        console.log(data)
        const selectTag = document.getElementById('conferences');
        for(let conference of data.conferences){
            const option = document.createElement('option');
            option.value = conference.id;
            option.innerHTML = conference.name;
            selectTag.append(option);
        };
    };

    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const conferenceId = Object.fromEntries(formData).conferences
    const presenter_name = Object.fromEntries(formData).presenter_name;
    const presenter_email = Object.fromEntries(formData).presenter_email;
    const company_name = Object.fromEntries(formData).company_name;
    const title = Object.fromEntries(formData).title;
    const synopsis = Object.fromEntries(formData).synopsis;
    const content = {
        presenter_name: presenter_name,
        presenter_email: presenter_email,
        company_name: company_name,
        title: title,
        synopsis: synopsis,

    }
    const json = JSON.stringify(content);
    console.log(json)
    const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
    },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
    formTag.reset();
    const newPresentation = await response.json();
    }
    else{
        console.log("this is an error")
    }

    })

});
