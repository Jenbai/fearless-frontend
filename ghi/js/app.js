function changeDate(date){
    date = new Date(date)
    return date.toLocaleDateString()
}

function createCard(title, description, pictureUrl, startDate, endDate, location){
    return `
    <div class = "col-md-4 col-md-4 mb-1">
        <div class = "shadow-lg p-3 mb-5 bg-body rounded">
            <div class="card" aria-hidden = "true">
                <img src="${pictureUrl}" class="card-img-top placeholder-glow">
                <div class="card-body placeholder-glow">
                <h5 class="card-title placeholder-glow">${title}</h5>
                <h6 class="card-subtitle mb-2 text-muted placeholder-glow">${location}</h6>
                <p class="card-text" placeholder-glow>${description}</p>
                <div class="card-footer" placeholder-glow>${startDate} - ${endDate}</div>
                </div>
            </div>
        </div>
    </div>
  `;
}
function handleError(){
    return `
    <div class="alert alert-danger" role="alert"> Error! No data loading! </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);

        if(!response.ok){
            throw new Error('Response not ok')

        } else{
            const data = await response.json();
            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = changeDate(details.conference.starts);
                    const endDate = changeDate(details.conference.ends);
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }
            }
        }catch (e) {
            console.log(e)
            const errorWarning = handleError()
            const errorTag = document.querySelector('.error')
            errorTag.innerHTML = errorWarning
    }
});
